machine à remplir des informations de COS (marCOS) :

Petit script Python permettant de lire un fichier XLS contenant les infos sur les candidats à des postes dans des établissement publics de l'Enseignement Supérieur et la Recherche, et préremplir des rapports sur ces candidats. Le script prend en entrée un fichier xls (voir le fichier de test inclu dans le repo) et génère un fichier PDF par candidat.

Ce script requiert
Python >= 3 
Jupyter Lab
numpy
pandas
pdflatex
